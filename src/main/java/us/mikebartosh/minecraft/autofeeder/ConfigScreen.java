package us.mikebartosh.minecraft.autofeeder;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.api.ConfigEntryBuilder;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.Text;

import java.util.List;

public class ConfigScreen {
    public static Screen createConfigScreen(final Screen parent, final Values config) {
        ConfigHolder<Values> autoConfig = AutoConfig.getConfigHolder(Values.class);
        final ConfigBuilder builder = ConfigBuilder.create().setParentScreen(parent).setTitle(Text.of("Auto Feeder Configuration"));
        ConfigCategory general = builder.getOrCreateCategory(Text.of("Auto Feeder - Main Config"));
        ConfigCategory listing = builder.getOrCreateCategory(Text.of("Auto Feeder - Listing Config"));

        ConfigEntryBuilder entryBuilder = builder.entryBuilder();
        general.addEntry(entryBuilder.startBooleanToggle(Text.of("Mod Enabled"), config.mod_enabled)
                .setDefaultValue(true)
                .setTooltip(Text.of("Enable or disable the mod"))
                .setSaveConsumer((newValue) -> config.mod_enabled = newValue)
                .build());

        general.addEntry(entryBuilder.startBooleanToggle(Text.of("Feed Babies"), config.feed_babies)
                .setDefaultValue(false)
                .setTooltip(Text.of("Enable or disable feeding babies"))
                .setSaveConsumer((newValue) -> config.feed_babies = newValue)
                .build());

        general.addEntry(entryBuilder.startBooleanToggle(Text.of("Feed Adults"), config.feed_adults)
                .setDefaultValue(true)
                .setTooltip(Text.of("Enable or disable feeding adults"))
                .setSaveConsumer((newValue) -> config.feed_adults = newValue)
                .build());

        general.addEntry(entryBuilder.startIntSlider(Text.of("Feeding Range"), config.feed_range, 0, 5)
                .setDefaultValue(6)
                .setTooltip(Text.of("The range in which to feed entities\nThe range is in blocks and the area it creates is a cube centered around the player"))
                .setSaveConsumer((newValue) -> config.feed_range = newValue)
                .build());

        general.addEntry(entryBuilder.startBooleanToggle(Text.of("Enable Interval"), config.enable_interval)
                .setDefaultValue(false)
                .setTooltip(Text.of("Enable or disable the interval\nIf enabled, the mod will only feed entities every x seconds\nIf disabled, the mod will feed entities every time the key is pressed/held"))
                .setSaveConsumer((newValue) -> config.enable_interval = newValue)
                .build());

        general.addEntry(entryBuilder.startIntField(Text.of("Click Interval"), config.click_interval)
                .setDefaultValue(1)
                .setTooltip(Text.of("Value in seconds for the interval"))
                .setSaveConsumer((newValue) -> config.click_interval = newValue)
                .build());

        listing.addEntry(entryBuilder.startBooleanToggle(Text.of("Listing Mode"), config.is_whitelist_mode)
                .setDefaultValue(false)
                .setYesNoTextSupplier((bool) -> bool ? Text.of("Whitelist") : Text.of("Blacklist"))
                .setTooltip(Text.of("Choose between blacklist or whitelist mode\nBlacklist mode will exclude entities from feeding\nWhitelist mode will only include entities from feeding"))
                .setSaveConsumer((newValue) -> config.is_whitelist_mode = newValue)
                .build());

        listing.addEntry(entryBuilder.startStrList(Text.of("Blacklist"), List.of(config.blacklist))
                .setDefaultValue(List.of())
                .setTooltip(Text.of("List of entities to exclude from feeding"))
                .setSaveConsumer((newValue) -> config.blacklist = newValue.toArray(new String[0]))
                .build());

        listing.addEntry(entryBuilder.startStrList(Text.of("Whitelist"), List.of(config.whitelist))
                .setDefaultValue(List.of())
                .setTooltip(Text.of("List of entities to include in feeding"))
                .setSaveConsumer((newValue) -> config.whitelist = newValue.toArray(new String[0]))
                .build());

        // We want to go over every entry in the whitelist and blacklist and make them all lowercase
        for (int i = 0; i < config.blacklist.length; i++) {
            config.blacklist[i] = config.blacklist[i].toLowerCase();
        }

        for (int i = 0; i < config.whitelist.length; i++) {
            config.whitelist[i] = config.whitelist[i].toLowerCase();
        }

        autoConfig.save();

        return builder.build();
    }
}
