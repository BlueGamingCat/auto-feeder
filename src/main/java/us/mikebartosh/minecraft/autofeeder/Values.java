package us.mikebartosh.minecraft.autofeeder;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;

@Config(name = "auto-feeder")
public class Values implements ConfigData {
    public Boolean mod_enabled = true;
    public Boolean feed_babies = false;
    public Boolean feed_adults = true;
    public Integer feed_range = 5;
    public Boolean is_whitelist_mode = false;
    public String[] blacklist = {};
    public String[] whitelist = {};
    public Boolean enable_interval = false;
    public Integer click_interval = 1;

    public Long last_action_time = 0L;
}
