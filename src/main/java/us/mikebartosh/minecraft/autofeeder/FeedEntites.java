package us.mikebartosh.minecraft.autofeeder;

import me.shedaniel.autoconfig.AutoConfig;
import net.minecraft.entity.passive.PassiveEntity;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.math.Box;
import net.minecraft.world.World;

import java.util.Arrays;
import java.util.Locale;

public class FeedEntites {
    public static void feedEntites() {
        Values config = AutoConfig.getConfigHolder(Values.class).getConfig();
        MinecraftClient mc = MinecraftClient.getInstance();
        World world = mc.world;
        PlayerEntity player = mc.player;

        if (world != null && player != null) {
            double playerX = player.getX();
            double playerY = player.getY();
            double playerZ = player.getZ();
            double range = config.feed_range;
            Box box = new Box(playerX - range, playerY - range, playerZ - range,
                              playerX + range, playerY + range, playerZ + range);

            for (Entity entity : world.getOtherEntities(null, box, EntityPredicates.VALID_ENTITY)) {
                if (!(entity instanceof PlayerEntity)) {
                    rightClickEntity(entity, mc, player);
                }
            }
        }
    }

    private static void rightClickEntity(Entity entity, MinecraftClient mc, PlayerEntity player) {
        Values config = AutoConfig.getConfigHolder(Values.class).getConfig();

        // If we don't want to feed babies and the entity is a baby, return
        if ((entity instanceof PassiveEntity pe && pe.isBaby() && !config.feed_babies)) {
            return;
        }

        // If we don't want to feed adults, we return
        if (entity instanceof PassiveEntity pe && !pe.isBaby() && !config.feed_adults) {
            return;
        }

        // We want to trim off the first 17 characters of the entity name
        // This is because the entity name is in the format of "entity.minecraft." + entity name
        final String entityName = entity.getType().toString().substring(17).toLowerCase(Locale.ROOT);

        // If we are on a blacklist and the entity is in the blacklist, return
        if (Arrays.asList(config.blacklist).contains(entityName) && !config.is_whitelist_mode) {
            return;
        }

        // If we are on a whitelist and the entity is not in the whitelist, return
        if (!Arrays.asList(config.whitelist).contains(entityName) && config.is_whitelist_mode) {
            return;
        }

        Hand hand = Hand.MAIN_HAND;
        ActionResult actionResult = mc.interactionManager.interactEntityAtLocation(player, entity, new EntityHitResult(entity), hand);

        if (!actionResult.isAccepted()) {
            actionResult = mc.interactionManager.interactEntity(player, entity, hand);
        } else {
            player.swingHand(hand);
        }
    }
}
