package us.mikebartosh.minecraft.autofeeder;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;

public class AutoFeeder implements ModInitializer {
    final KeyBinding feedEntities = KeyBindingHelper.registerKeyBinding(new KeyBinding("Feed Entities", InputUtil.UNKNOWN_KEY.getCode(), "Auto Feeder"));

    @Override
    public void onInitialize() {
        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            ConfigHolder<Values> autoConfig = AutoConfig.getConfigHolder(Values.class);
            Values config = autoConfig.getConfig();

            if (config.mod_enabled) {
                if (config.enable_interval) {
                    if (feedEntities.isPressed()) {
                        long currentTime = System.currentTimeMillis();
                        if (currentTime - config.last_action_time >= config.click_interval * 1000) {
                            FeedEntites.feedEntites();
                            config.last_action_time = currentTime;
                            autoConfig.save();
                        }
                    } else {
                        config.last_action_time = 0L;
                        autoConfig.save();
                    }
                } else {
                    if (feedEntities.wasPressed()) {
                        FeedEntites.feedEntites();
                    }
                }
            }
        });
    }
}
