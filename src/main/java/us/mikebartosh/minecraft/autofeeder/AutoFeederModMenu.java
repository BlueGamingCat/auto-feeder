package us.mikebartosh.minecraft.autofeeder;

import com.terraformersmc.modmenu.api.ConfigScreenFactory;
import com.terraformersmc.modmenu.api.ModMenuApi;
import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.ConfigHolder;
import me.shedaniel.autoconfig.serializer.Toml4jConfigSerializer;

public class AutoFeederModMenu implements ModMenuApi {
    @Override
    public ConfigScreenFactory<?> getModConfigScreenFactory() {
        ConfigHolder<Values> autoConfig = AutoConfig.register(Values.class, Toml4jConfigSerializer::new);
        return parent -> ConfigScreen.createConfigScreen(parent, AutoConfig.getConfigHolder(Values.class).getConfig());
    }
}
